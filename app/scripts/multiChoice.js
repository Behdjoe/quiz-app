'use strict';
/** @jsx React.DOM */
/**
 * Created by behdjoe on 26/02/15.
 */
var React = require('react'),
    XHR = require('./utils/xhr'), //This is a utility for making XHR calls.
    Store = {}; //this is the Store set in a global scope.


var Question = React.createClass({

    //TODO: Randomize answers order to a question.

    myArr: {},
    /**
     * This method is bound to the input.
     * When the user clicks an answer, the selected answer gets stored.
     * @param event
     */
    handleChange: function (event) {
      this.myArr[event.target.name] = event.target.value;
      Store.selectedAnswers = this.myArr;
    },

    /**
     * this is the render method for the Question component.
     * @returns {XML}
     */
    render: function () {
      var self = this;
      var answerList = [];
      this.props.answers.forEach(function (answer) {
        answerList.push(
          <li key={self.props.id}>
            <input
              id={answer.id}
              type="radio"
              name={self.props.questionId}
              value={answer.id}
              onChange={self.handleChange}
            />
            <label htmlFor={answer.id}>{answer.text}</label>
          </li>)
      });
      return (
        <div className="fadeInUp animated question">
          <h3>{self.props.title}</h3>
          <p>{self.props.question}</p>
          <ol type="A" className="answer-list">
            {answerList}
          </ol>
        </div>
      )
    }
  }),

    MultiChoice = React.createClass({

    /**
     * This will set the initial state of the component
     */
    getInitialState: function () {
      this.requestQuestions();
      return {
        hideForm: '',
        testId: null,
        testTitle: null,
        questions: null,
        totalScore: ''
      };
    },

    /**
     * This method will make an XHR call to get the questions.
     */
    requestQuestions: function () {
      var self = this;
      var url = '/scripts/json/TEST-1/questions.json',
          method = 'GET';

      new XHR({
        url: url,
        method: method,
        onSuccess: function (event) {
          var me = this,
              response = JSON.parse(me.responseText);

          /**
           * here we set the resonse in the state.
           */
          self.setState({
            testId: response.id,
            testTitle: response.title,
            testSubtitle: response.subtitle,
            questions: response.questions
          });
        },
        onError: function (err) {
          console.error(err);
        }
      });
    },

    /**
     * This will make a request to get the answers.
     * It takes a callback as a parameter.
     * @param callback
     * @returns {boolean}
     */
    requestAnswers: function (callback) {
      var self = this;
      var url = '/scripts/json/TEST-1/answers.json',
        method = 'GET';

      new XHR({
        url: url,
        method: method,
        onSuccess: function (event) {
          var me = this,
              response = JSON.parse(me.responseText);
          self.setState({
            solutionsId: response.id,
            solutions: response.solutions
          });

          callback();
        },
        onError: function (err) {
          console.error(err);
        }
      });

      // break if the testID doesn't match the solutionsID.
      if (self.state.testId !== self.state.solutionsId) {return false}
    },

    /**
     * This is the method that is called to compute the score.
     */
    computeScore: function () {
      var allSolutions = this.state.solutions,
        selectedAnswers = Store.selectedAnswers,
        totalScore = 0;

      /**
       * loop through allSolutions and see if they match
       * the selected answers.
       */
      for (var key in allSolutions) {
        var selectedAnswer = selectedAnswers[key],
          AnswerPoints = allSolutions[key].answers[selectedAnswer],
          AnswerWeight = allSolutions[key].value;

        //check to see if the selected answer is not 0 or undefined
        if (AnswerPoints !== undefined) {

          //the total score wil be calculated here inside the loop.
          totalScore += (AnswerWeight / 100) * AnswerPoints;
        }
      }

      this.setState({
        totalScore: totalScore,
        hideForm: 'hide'
      });
    },

    /**
     * the user clicks the submit button, this method calls the other methods.
     * @param event
     */
    handleSubmit: function (event) {

      // this will prevent the html form from actually submitting.
      event.preventDefault();

      // We call requestAnswers, and send computeScore as callback.
      this.requestAnswers(this.computeScore);
    },

    /**
     * this is the render metod for the MultiChoice component.
     * @returns {XML}
     */
    render: function () {
      var self = this,
          testCompleted;
      var formClassNames = 'test-form pure-form '+ this.state.hideForm;

      if (!this.state.questions) {
        return (<div></div>)
      }

      /**
       * this will generate an array of <question>'s
       * @type {Array}
       */
      var questionList = []; //the array is stored inside here.
      this.state.questions.forEach(function (question) {
        questionList.push(
          <Question
            key={question.id}
            questionId={question.id}
            title={question.title}
            question={question.text}
            answers={question.options}
          />);
      });

      //TODO: Show the selected answers, alsong side the correct answers, after the user submits.

      /**
       * Here we do some comparisons and determine what the user sees,
       * after clicking the submit buttin.
       */
      if (self.state.totalScore || self.state.totalScore === 0) {
        var totalScore = self.state.totalScore;

        if (totalScore < 50) {
          testCompleted = (
            <div className="test-completed">
              <h2>Test Completed.</h2>
              <p>Looks like you got a score of <b>{totalScore}%</b>.</p>
              <p>Better luck next time!</p>
              <a href="/" className="pure-button pure-button-primary">Start over</a>
              <p><img src="images/crying-panda.png" /></p>
            </div>
          );
        }
        else if (totalScore > 51 && totalScore < 99){
          testCompleted = (
            <div className="test-completed">
              <h2>Test Completed.</h2>
              <p>Well... It looks like you only got a score of <b>{totalScore}%</b>.</p>
              <p>Not bad, try a little harder next time</p>
              <a href="/" className="pure-button pure-button-primary">Start over</a>
              <p><img src="images/facepalm-bear.png" /></p>
            </div>
          );
        }

        else if (totalScore === 100){
          testCompleted = (
            <div className="test-completed">
              <h2>Test Completed.</h2>
              <p>Good job! you got a score of <b>{totalScore}%</b>.</p>
              <p>Here are some funny pictures.</p>
              <a href="/" className="pure-button pure-button-primary">Start over</a>
              <p><img className="pure-img" src="images/titanic.jpg" /></p>
              <p><img className="pure-img" src="images/seen-dog.jpg" /></p>
              <p><img className="pure-img" src="images/css-mug.jpg" /></p>
              <p><img className="pure-img" src="images/code.jpg" /></p>
              <p><img className="pure-img" src="images/seo.gif" /></p>
              <p><img className="pure-img" src="images/beard.jpg" /></p>
              <p><img className="pure-img" src="images/code-not-working.jpg" /></p>
              <p><img className="pure-img" src="images/devices.jpg" /></p>
              <p><img className="pure-img" src="images/diary.jpg" /></p>
              <p><img className="pure-img" src="images/crying-programmers.jpg" /></p>
            </div>
          );
        }
      }

      /**
       * this is where all the actual rendering happens (return).
       */
      return (
        <div>
          <div className="test-title">
            <h2>{this.state.testTitle}</h2>
            <p>{this.state.testSubtitle}</p>
          </div>
          <div className="test">
          <form name="test-form" className={formClassNames} onSubmit={this.handleSubmit}>
            {questionList}
            <label className="pure-label"> Click submit to complete the test</label> <button className="pure-button pure-button-primary" type="submit">Submit</button>
          </form>
            {testCompleted}
          </div>
        </div>
      )
    }

  });

module.exports = MultiChoice;

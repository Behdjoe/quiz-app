'use strict';

/**
 * An XHR and/or XDR (IE) wrapper
 *
 * @class XHR
 * @constructor
 * @param {Object} options Options object to configure the XHR request
 *     @param {String} options.url URL to be used in the request
 *     @param {Function} options.onSuccess Callback to be executed when the request responds with a positive HTTP code
 *     @param {Function} options.onError Callback to be executed when the request responds with a negative HTTP code or
 *     had a problem and could not be resolved
 *
 *     @param {String} [options.method] Optional. Method to be used in the request. Default value is GET. Possible
 *     values: GET, POST, DELETE, PUT
 *     @param {Object} [options.data] Optional. When POSTing or PUTing, you can pass an object to be sent in the request
 *     @param {Object} [options.headers] Optional. Object with 'custom headers' to be set on the request
 *     @param {Function} [options.onProgress] Optional. Callback to be executed during the request to track the progress
 *     @param {Function} [options.onTimeout] Optional. Callback to be executed when the request timesout.
 *
 * @return {Object} An instance of the XHR class.
 * @public
 *
 * @example
 *   var XHR = require('xhr.utility.js');
 *   var ajaxRequest = new XHR({
 *     method: 'GET',
 *     onSuccess: function( event ){
 *       console.log(this.responseText);
 *     },
 *     onError: function( err ){
 *       console.error(err);
 *     }
 *   });
 *
 *  var ajaxPOSTRequest = new XHR({
 *    method: 'POST',
 *    url: config.apiUrl,
 *    headers:{
 *      'Content-Type': 'application/json'
 *    },
 *    data: {
 *      test: 1,
 *      test2: 2
 *    },
 *    onSuccess: function(){
 *      console.log( 'Success', this.responseText );
 *    },
 *    onError: function( e ){
 *      console.log( 'Error', e );
 *    }
 *  });
 */

var XHR = function(options){
  var that = this,
      dataToBePosted = null;

  /** @type {bool} Flag that determines if it should be used the XDR or not */
  this._isXDR = ('XDomainRequest' in window);

  /** @type {XMLHttpRequest|XDomainRequest} The browser's native object to make the AJAX requests */
  this._native = this._isXDR ? new window.XDomainRequest() : new window.XMLHttpRequest();

  options.method = (options.method||'GET').toUpperCase();

  this._native.open( options.method, options.url );

  if( (options.method === 'POST') && ('data' in options) ){
    dataToBePosted = JSON.stringify( options.data );
  }

  if( 'headers' in options ) {
    this._setHeaders( options.headers );
  }

  this._native.onload = function( event ) {
    if( (this.readyState === 4) && (['1','2','3'].indexOf(this.status.toString()[0]) !== -1) ) {
      options.onSuccess.call(this, event );
    } else {
      options.onError.call(this, event );
    }
  };

  this._native.onerror = function( err ) {
    options.onError.call(this, err );
  };


  if( this._isXDR ){
    this._initializeXDR( options );
    setTimeout(
      function(){ that._native.send(); }
    );
  } else {
    this._native.send( dataToBePosted );
  }
};

XHR.prototype = {

  /**
   * Sets the headers defined in the options passed to the construct, in the headers property.
   *
   * @method _setHeaders
   * @param {Object} headers A key-value object containing headers to be set in the request
   * @return {void}
   * @private
   */
  _setHeaders: function( headers ) {
    for( var key in headers ) {
      if( headers.hasOwnProperty(key) ){
        this._native.setRequestHeader(key, headers[key]);
      }
    }
  },

  /**
   * In case we're using an XDomainRequest, this type of object requires that the 'onprogress' and 'ontimeout' functions
   * are defined. That's what this method does.
   *
   * @method _initializeXDR
   * @param {Object} options Options' object passed to the constructor.
   * @return {void}
   * @private
   */
  _initializeXDR: function( options ){
    this._native.onprogress = options.onProgress || function(){};
    this._native.ontimeout = options.onTimeout || function(){};
  }
};

module.exports = XHR;

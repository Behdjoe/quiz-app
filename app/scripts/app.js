'use strict';
/**
 * Created by behdjoe on 26/02/15.
 */
var React = require('react'),
    mountNode = document.getElementById("app"),
    MultiChoice = require('./multiChoice');

React.renderComponent(<MultiChoice />, mountNode);

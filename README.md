# README #
This is a multiple-choice quiz app that I created. It uses React, Sass, Gulp and many other techniques.

###You will need:###
- [NodeJS](http://nodejs.org)

### How do I get set up? ###
Fire up a terminal, go to the folder and type:

```
#!node
$ npm install  #Installs the dependencies.

$ gulp build # this will build the app

$ gulp watch  #This starts the watchers and the server
```
### Screenshot ###
![Screenshot 2015-03-01 16.58.58.PNG](https://bitbucket.org/repo/jn6zko/images/1139784989-Screenshot%202015-03-01%2016.58.58.PNG)
